﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Movie_Characters_WebAPI.Models
{
    public class Character
    {
        [Required]
        [Key]
        public int CharacterId { get; set; }

        [MaxLength(50)]
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureLink { get; set; }

        //Relation(many to many)
        public ICollection<Movie> Movie { get; set; }

    }
}
