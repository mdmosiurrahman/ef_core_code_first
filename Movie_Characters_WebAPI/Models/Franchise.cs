﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Movie_Characters_WebAPI.Models
{
    public class Franchise
    {
        [Required]
        [Key]
        public int FranchiseId { get; set; }
        [MaxLength(50)]
        public string FranchiseName { get; set; }
        public string  Description { get; set; }

        public ICollection<Movie> Movie { get; set; }


    }
}
