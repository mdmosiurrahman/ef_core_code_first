﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movie_Characters_WebAPI.Models
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }
        public DbSet<Movie> Movie { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC1695\\SQLEXPRESS; Initial Catalog=MovieCharacter; Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 1, FranchiseName = "Paramount", Description = "World famous movie franchiser " });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 2, FranchiseName = "Star group", Description = "Big company " });

            
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 1, FullName = "Superman", Alias = "Power with S", Gender = "Male", PictureLink = "https://i.pinimg.com/originals/15/cb/f5/15cbf5fb4f4449569815e9267f9e6d3c.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 2, FullName = "Batman", Alias = "Power with B", Gender = "Male", PictureLink = "https://i.pinimg.com/originals/15/cb/f5/15cbf5fb4f4449569815e9267f9e6d3c.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 3, FullName = "Spiderman", Alias = "Power with Net", Gender = "Male", PictureLink = "https://i.pinimg.com/originals/15/cb/f5/15cbf5fb4f4449569815e9267f9e6d3c.jpg" });

            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 1, Title = "Superman on Earth", ReleaseYear =2019, Director = "Mosiur", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 2, Title = "Batman Return", ReleaseYear = 2018, Director = "Rahman", FranchiseId = 2 });
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 3, Title = "Spiderman on Rise", ReleaseYear = 2017, Director = "Sohel", FranchiseId = 1 });

            modelBuilder.Entity<Character>()
                .HasMany(x => x.Movie)
                .WithMany(y => y.Character)
                .UsingEntity<Dictionary<string, object>>(
                "CharaterMovie",
                p =>p.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                m =>m.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                n =>
                {
                    n.HasKey("CharacterId", "MovieId");
                    n.HasData(
                        new { MovieId = 1, CharacterId = 2 },
                        new { MovieId = 1, CharacterId = 3 },
                        new { MovieId = 3, CharacterId = 1 },
                        new { MovieId = 2, CharacterId = 2 },
                        new { MovieId = 2, CharacterId = 3 }

                        );
                }
                );
        }
    }
}
