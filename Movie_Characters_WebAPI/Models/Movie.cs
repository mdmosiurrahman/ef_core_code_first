﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Movie_Characters_WebAPI.Models
{
    public class Movie
    {
        [Required]
        [Key]
        public int MovieId  { get; set; }
        [MaxLength(50)]
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string  Picture { get; set; }
        public string TrailerLink { get; set; }

        // Relation(many to many)
        public ICollection<Character> Character { get; set; }
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }

    }
}
