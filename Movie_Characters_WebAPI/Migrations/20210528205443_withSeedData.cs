﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Movie_Characters_WebAPI.Migrations
{
    public partial class withSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "CharacterId", "Alias", "FullName", "Gender", "PictureLink" },
                values: new object[,]
                {
                    { 1, "Power with S", "Superman", "Male", "https://i.pinimg.com/originals/15/cb/f5/15cbf5fb4f4449569815e9267f9e6d3c.jpg" },
                    { 2, "Power with B", "Batman", "Male", "https://i.pinimg.com/originals/15/cb/f5/15cbf5fb4f4449569815e9267f9e6d3c.jpg" },
                    { 3, "Power with Net", "Spiderman", "Male", "https://i.pinimg.com/originals/15/cb/f5/15cbf5fb4f4449569815e9267f9e6d3c.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "FranchiseId", "Description", "FranchiseName" },
                values: new object[,]
                {
                    { 1, "World famous movie franchiser ", "Paramount" },
                    { 2, "Big company ", "Star group" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "TrailerLink" },
                values: new object[] { 1, "Mosiur", 1, null, null, 2019, "Superman on Earth", null });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "TrailerLink" },
                values: new object[] { 3, "Sohel", 1, null, null, 2017, "Spiderman on Rise", null });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "TrailerLink" },
                values: new object[] { 2, "Rahman", 2, null, null, 2018, "Batman Return", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "CharacterId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "CharacterId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "CharacterId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "FranchiseId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "FranchiseId",
                keyValue: 2);
        }
    }
}
